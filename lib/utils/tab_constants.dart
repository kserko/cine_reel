enum TabKey { kNowPlaying, kTopRated, kPopular }

const Map<TabKey, String> tab = {
  TabKey.kNowPlaying: "Now Playing",
  TabKey.kTopRated: "Top Rated",
  TabKey.kPopular: "Popular"
};
